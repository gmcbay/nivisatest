package main

import (
	"bitbucket.org/gmcbay/nivisa"
	"fmt"
	"os"
)

func main() {
	var (
		esp300 *nivisa.Esp300
		err    error
	)

	if esp300, err = nivisa.NewEsp300("GPIB0::1::INSTR"); err != nil {
		fmt.Printf("err: %v\n", err)
		os.Exit(1)
	}

	fmt.Printf("esp300: %v\n", esp300)

	// Was
	// Axis1: 8.49998
	// Axis2: 7.09998
	// Axis3: 4.29999

	// Move all 3 axes to original positions
	esp300.MoveToAbsPos(1, 8.49998)
	esp300.MoveToAbsPos(2, 7.09998)
	esp300.MoveToAbsPos(3, 4.29999)
}
